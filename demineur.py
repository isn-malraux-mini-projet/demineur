#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 25 10:53:39 2018

@author: utilisateur
"""


import tkinter
import random
from PIL import Image

imagePerdant=Image.open("Game_Over1.png")
imageGagnant=Image.open("bravo1.jpg")
terrainMines = []
nb_col_Jeu = 5
nb_lgn_Jeu = 5
nbclic =  0
nbMines = 10
nbClicTotal = nb_col_Jeu * nb_lgn_Jeu - nbMines

def CreeTerrainMine(nb_col_Jeu, nb_lgn_Jeu): # ca cree une liste qui pour chaque range entre des nombres aleatoires pour chaque colonne.
    terrainMines = [[0 for colonne in range(nb_col_Jeu)]for ligne in range(nb_lgn_Jeu)]
    liste_Bombes=[]
    while len(liste_Bombes)<10:
        n = random.randint(0,nb_lgn_Jeu*nb_col_Jeu-1) # calcul un nombre aleatoire entre 0 et 63
        if not n in liste_Bombes:
            liste_Bombes.append(n)
            ligne=n//nb_lgn_Jeu
            colonne=n% nb_col_Jeu
            terrainMines[ligne][colonne]=9 # positionne la bombe
    #print(liste_Bombes) #faire calcul des bombes voisines
    for ligne in range (nb_lgn_Jeu):
        for colonne in range (nb_col_Jeu):
             if terrainMines[ligne][colonne]==9:#on regarde les 8cases autour et on ajoute 1
                 for lgn in range(ligne-1,ligne+2):
                     for col in range(colonne-1,colonne+2):#+2 car le 2 est exclus de l'encadrement.
                         if 0<=lgn<nb_lgn_Jeu and 0<=col<nb_col_Jeu and terrainMines[lgn][col]!=9:
                             terrainMines[lgn][col]=terrainMines[lgn][col]+1
    for lgn in terrainMines:
        print(lgn)
    return terrainMines

    
def bouton(Frame1, terrainMines): # permettent de proposer une action à l'utilisateur. Affichage du tableau a l ecran
    for nbRangee in range(nb_lgn_Jeu):
        for nbColonne in range(nb_col_Jeu):
            carre = tkinter.Button(Frame1, text=terrainMines[nbRangee][nbColonne],height=5, width=7, fg="snow3",bg="snow3", activebackground="snow3",activeforeground="snow3") #bouton ca créer la forme carré. bg c'est background c'est la couleur de fond.  
            carre.grid(row = nbRangee, column = nbColonne ) 
            carre.bind ("<Button-1>", boutonClic) #permet de lier la fonction boutonClic au clic gauche de la souris
                       
def boutonClic(event): 
    bouton = event.widget 
    conditionExplosion(bouton)
    
def conditionExplosion(bouton): # En fonction des valeurs des cases, soit le jeu est terminé, soit il continu.
    valeurBouton = bouton.cget("text") # permet de récupérer la valeur du bouton 
    #print (valeurBouton)
    if (valeurBouton == 9): # c'est une bombe
    # affichage de l'image perdant
      imagePerdant.show()
      exit(1)
    else: # ce n'est pas une bombe
        couleurPolice = bouton.cget("fg") #permet de récupérer la couleur du texte du bouton
        if (couleurPolice == 'snow3'): # couleur inchangée : on n'a jamais clicqué sur le bouton
             global nbclic 
             nbclic += 1 # incremente le nombre de clic
             bouton.configure(fg="white",bg='Royalblue1') # change la couleur de la police et du fond du bouton
        if (nbclic == nbClicTotal): # toutes les cases sans bombe sont cochées
           #affichage de l'image gagnant
           imageGagnant.show()
           exit(0)
                 
  
def jouerDemineur():

    fenetre = tkinter.Tk() 
    fenetre.title("Jeu du démineur")    
    Frame1 = tkinter.Frame(fenetre, borderwidth=2, bg="white", relief=tkinter.GROOVE)
    Frame1.pack()
    terrainMine = CreeTerrainMine(nb_col_Jeu, nb_lgn_Jeu)
    bouton(Frame1,terrainMine)
    fenetre.mainloop()
     
if __name__ == "__main__":
    jouerDemineur()
